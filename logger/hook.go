package logger

import (
	"fmt"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"time"
)

func rotateLogsHook(appName string) *lfshook.LfsHook {
	writer, _ := rotatelogs.New(
		fmt.Sprintf("./storage/logs/%s_%%Y-%%m-%%d.log", appName),
		rotatelogs.WithRotationTime(time.Hour*24),
		rotatelogs.WithMaxAge(time.Hour*24*180))

	writerMap := lfshook.WriterMap{
		logrus.PanicLevel: writer,
		logrus.FatalLevel: writer,
		logrus.ErrorLevel: writer,
		logrus.InfoLevel:  writer,
		logrus.WarnLevel:  writer,
		logrus.DebugLevel: writer,
	}

	lfsHook := lfshook.NewHook(writerMap, &logrus.JSONFormatter{})
	return lfsHook
}

package config

import (
	"gin-web/funcmap"
	"html/template"
)

var FuncMap = template.FuncMap{
	"TimeFormat": funcmap.TimeFormat,
}

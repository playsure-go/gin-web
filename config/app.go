package config

const (
	AppEnv   string = "local"
	AppName  string = "gin-web"
	LogLevel string = "debug"
)

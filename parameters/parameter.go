package parameters

type Response struct {
	ErrorCode ErrorCode `json:"error_code" binding:"required"`
}

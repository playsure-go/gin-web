package routes

import (
	"gin-web/controllers/web"
	"gin-web/middleware"
	"github.com/gin-gonic/gin"
)

func RouteWeb(parent *gin.Engine) {
	router := parent.Group("/")
	{
		router.Use(middleware.Sessions())
		router.Use(middleware.Csrf())

		// Inserts web routes here.
		router.GET("/", web.HomeIndex)
	}
}

package routes

import (
	"gin-web/controllers/api/get_version"
	"github.com/gin-gonic/gin"
)

func RouteApi(parent *gin.Engine) {
	router := parent.Group("/api")
	{
		// router.Use(middleware.Cors())

		// Inserts API routes here.
		router.GET("/version", get_version.Handle)
	}
}

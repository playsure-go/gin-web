package writer

import (
	"fmt"
	"gin-web/funcmap"
	"log"
	"os"
	"path"
	"time"
)

func GinLogWriter(appName string) *os.File {
	dir, err := os.Getwd()
	if err != nil {
		log.Println(err.Error())
	}

	filePath := path.Join(dir, "/storage/logs/")
	if err2 := os.MkdirAll(filePath, 0775); err2 != nil {
		log.Println(err2.Error())
		return nil
	}

	date := funcmap.DateFormat(time.Now())
	fileName := fmt.Sprintf("%s_access_%s.log", appName, date)

	logName := path.Join(filePath, fileName)
	if _, err3 := os.Stat(logName); err3 != nil {
		if _, err4 := os.Create(logName); err4 != nil {
			log.Println(err4.Error())
			return nil
		}
	}

	file, err5 := os.OpenFile(logName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err5 != nil {
		log.Println("err", err5)
		return nil
	}

	return file
}

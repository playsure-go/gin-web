package env_config

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type EnvConfig struct {
	Values []string
}

func (ec *EnvConfig) Add(value string) {
	ec.Values = append(ec.Values, value)
}

func (ec *EnvConfig) Set(key string, value string) {
	vLen := len(ec.Values)
	for i := 0; i < vLen; i++ {
		v := ec.Values[i]
		if strings.HasPrefix(v, key) {
			ec.Values[i] = fmt.Sprintf("%s=%s", key, value)
			return
		}
	}
	ec.Add(fmt.Sprintf("%s=%s", key, value))
}

func (ec *EnvConfig) Get(key string) string {
	vsLen := len(ec.Values)
	for i := 0; i < vsLen; i++ {
		v := ec.Values[i]
		pre := strings.HasPrefix(v, key)
		sepIdx := strings.Index(v, "=")
		kLen := len(key)
		if pre && sepIdx >= kLen {
			value := v[sepIdx+1:]
			value = strings.TrimSpace(value)
			value = strings.Trim(value, "\"")
			return value
		}
	}
	return ""
}

func (ec *EnvConfig) Load(filename string) error {
	file, err := os.OpenFile(filename, os.O_RDONLY, os.FileMode(0644))
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		ec.Add(scanner.Text())
	}

	if err2 := scanner.Err(); err2 != nil {
		return err2
	}

	return nil
}

func (ec *EnvConfig) Save(filename string) error {
	file, err := os.OpenFile(filename, os.O_WRONLY, os.FileMode(0644))
	if err != nil {
		return err
	}
	defer file.Close()

	for _, value := range ec.Values {
		if _, err2 := file.WriteString(value + "\n"); err2 != nil {
			return err2
		}
	}

	return nil
}

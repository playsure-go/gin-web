package app_key

import (
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"strings"
)

type AppKeyType string

const (
	APP_KEY_TYPE_HEX    AppKeyType = "hex"
	APP_KEY_TYPE_BASE64 AppKeyType = "base64"
)

type AppKey struct {
	Type AppKeyType
	Data []byte
}

func Parse(key string) (*AppKey, error) {
	ss := strings.Split(key, ":")
	if len(ss) != 2 {
		return nil, errors.New("invalid app key format")
	}
	appKey := new(AppKey)
	appKey.Type = AppKeyType(ss[0])
	if appKey.Type == APP_KEY_TYPE_HEX {
		d, e := hex.DecodeString(ss[1])
		if e != nil {
			return nil, e
		}
		appKey.Data = d
	} else if appKey.Type == APP_KEY_TYPE_BASE64 {
		d, e := base64.StdEncoding.DecodeString(ss[1])
		if e != nil {
			return nil, e
		}
		appKey.Data = d
	} else {
		e := errors.New("invalid app key type")
		return nil, e
	}
	return appKey, nil
}

func (a AppKey) String() string {
	var key string
	if a.Type == APP_KEY_TYPE_HEX {
		key = hex.EncodeToString(a.Data)
	} else if a.Type == APP_KEY_TYPE_BASE64 {
		key = base64.StdEncoding.EncodeToString(a.Data)
	}
	appKey := fmt.Sprintf("%s:%s", a.Type, key)
	return appKey
}

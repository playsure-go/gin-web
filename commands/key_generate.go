package main

import (
	"bitbucket.org/playsure-go/generator"
	"fmt"
	"gin-web/logger"
	"gin-web/tools/app_key"
	"gin-web/tools/env_config"
)

func main() {
	appKey := new(app_key.AppKey)
	appKey.Type = app_key.APP_KEY_TYPE_BASE64
	appKey.Data = generator.Bytes(32)
	key := appKey.String()

	envFilename := ".env"
	envConfig := new(env_config.EnvConfig)
	if err := envConfig.Load(envFilename); err != nil {
		logger.Error(err.Error())
		fmt.Println(err.Error())
		return
	}
	envConfig.Set("APP_KEY", key)
	if err := envConfig.Save(envFilename); err != nil {
		logger.Error(err.Error())
		fmt.Println(err.Error())
	}
}

package middleware

import (
	"github.com/gin-gonic/gin"
	csrf "github.com/utrack/gin-csrf"
)

func Csrf() gin.HandlerFunc {
	return csrf.Middleware(csrf.Options{
		Secret:    "secret123",
		ErrorFunc: defaultCsrfErrorFunc,
	})
}

func defaultCsrfErrorFunc(c *gin.Context) {
	c.String(400, "CSRF token mismatch")
	c.Abort()
}

package middleware

import (
	"gin-web/tools/app_key"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func Sessions() gin.HandlerFunc {
	appKey := getAppKey()
	store := cookie.NewStore(appKey)
	return sessions.Sessions("my_session", store)
}

func getAppKey() []byte {
	key := viper.GetString("APP_KEY")
	appKey, e := app_key.Parse(key)
	if e != nil {
		return defaultAppKey()
	}
	return appKey.Data
}

func defaultAppKey() []byte {
	return []byte("your-32bytes-key") // 16 bytes.
}
